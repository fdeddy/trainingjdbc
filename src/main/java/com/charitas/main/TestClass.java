/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.charitas.main;

import com.charitas.dao.CategoryDAO;
import com.charitas.dao.ProductDAO;
import com.charitas.dao.impl.CategoryDAOImpl;
import com.charitas.dao.impl.ProductDAOImpl;
import com.charitas.db.DatabaseConnection;
import com.charitas.model.Category;
import com.charitas.model.Product;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author edepe1
 */
public class TestClass {
    public static void main(String[] args) {
        try {
            ProductDAO productDAO = new ProductDAOImpl(DatabaseConnection.getInstance().getConnection());
            Calendar cal = Calendar.getInstance();
            
            Category cat1 = new Category();
            cat1.setId(1);
            
            Product prod1 =new Product();
            prod1.setCategory(cat1);
            prod1.setDescription("tes prod");
            prod1.setExpiredDate(cal.getTime());
            prod1.setName("permen");
            prod1.setPrice(7500);
            productDAO.save(prod1);
                    
            Product product = new Product();
            List<Product> allData = productDAO.findAll();
            
            for(int i = 0; i < allData.size(); i++ ){
                Category cat = new Category();
                
                product = allData.get(i);
                cat = product.getCategory();
                        
                System.out.println("Id produk = " + product.getId() + "  " + product.getName() + "  " + cat.getId()+ "  " + cat.getName());
            }
            /*
            try {        
                CategoryDAO categoryDAO = new CategoryDAOImpl(DatabaseConnection.getInstance().getConnection());
                
                Category cat = new Category();
                cat.setName("mouseee ");
                categoryDAO.save(cat);
                
                List<Category> allData = categoryDAO.findAll();
                for(int i=0; i < allData.size(); i++ ){
                    //Category cat2 = allData.get(i);
                    cat = allData.get(i);
                    System.out.println("id = " + cat.getId() + "  name = " + cat.getName() );
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(TestClass.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(TestClass.class.getName()).log(Level.SEVERE, null, ex);
            }
            */
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestClass.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(TestClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        

        
    }
       
        
    
}
