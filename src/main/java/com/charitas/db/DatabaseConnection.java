/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.charitas.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author edepe1
 */
public class DatabaseConnection {
    private static DatabaseConnection instance;
    private Connection connection;
    private String url="jdbc:mysql://localhost:3306/dbtestjdbc";
    private String nama="root";
    private String pass="root";
    
    private DatabaseConnection() throws ClassNotFoundException{
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.connection = DriverManager.getConnection(url,nama,pass);
            
        } catch (SQLException ex) {
            System.out.println("database connection gagal " + ex.getLocalizedMessage());
        }
    }
    public Connection getConnection(){
        return connection;
    }
    
    public static DatabaseConnection getInstance() throws ClassNotFoundException, SQLException{
        if (instance == null ){
            instance = new DatabaseConnection();
        }else if (  instance.getConnection().isClosed() ){
            instance = new DatabaseConnection();
        }
        return instance;
    }
    
    
}

