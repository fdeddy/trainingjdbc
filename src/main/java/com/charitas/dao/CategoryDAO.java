/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.charitas.dao;

import com.charitas.model.Category;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author edepe1
 */
public interface CategoryDAO {

    int save(Category category) throws SQLException;

    int update(int i, Category category) throws SQLException;

    int delete(int i) throws SQLException;

    Category findById(int i) throws SQLException;

    List<Category> findAll() throws SQLException;

    List<Category> findByName(String name) throws SQLException;
    
}
