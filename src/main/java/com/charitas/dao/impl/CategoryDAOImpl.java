/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.charitas.dao.impl;

import com.charitas.dao.CategoryDAO;
import com.charitas.model.Category;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author edepe1
 */
public class CategoryDAOImpl implements CategoryDAO {

    private Connection connection;
    
    public CategoryDAOImpl (Connection connection ){
        this.connection = connection;
    }
    @Override
    public int save(Category category) throws SQLException {
        String sql ="Insert Into Category (name) values(?) ";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, category.getName());
        return statement.executeUpdate();
    }

    @Override
    public int update(int i, Category category) throws SQLException {
        String sql ="update Category set name =? where Category_id=? ";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, category.getName());
        statement.setInt(2, i);
        
        return  statement.executeUpdate();
        
    }

    @Override
    public int delete(int i) throws SQLException {
        String sql ="delete from Category where category_Id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1  , i);
        return statement.executeUpdate();                
    }

    @Override
    public Category findById(int i) throws SQLException {
        String sql ="select * from category where category_id = ? ";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, i);
        ResultSet result = statement.executeQuery();
        Category cat = new Category();
        if ( result.next() ){            
            cat.setId(result.getInt("category_id") );
            cat.setName(result.getString("name") );
        }
        return cat;
    }

    @Override
    public List<Category> findAll() throws SQLException {
        String sql = "Select * from category ";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet result = statement.executeQuery();
        Category cat ;
        List<Category> list = new ArrayList<Category>();
        while (result.next() ){
            cat = new Category();
            cat.setId(result.getInt("category_id")  );
            cat.setName(result.getString("name")  );
            list.add (cat);
        }                
        return list ;
    }

    @Override
    public List<Category> findByName(String name) throws SQLException {
        String sql ="select * from category where name = ? ";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet result = statement.executeQuery();
        
        Category cat;
        List<Category> list = new ArrayList<Category>();
        while (result.next()){
            cat = new Category();
            cat.setId(result.getInt("category_id"));
            cat.setName(result.getString("name"));
            list.add(cat);
        }
        return list;
    }
    
}
