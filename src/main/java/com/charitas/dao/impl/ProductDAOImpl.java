/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.charitas.dao.impl;

import com.charitas.dao.ProductDAO;
import com.charitas.model.Category;
import com.charitas.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author edepe1
 */
public class ProductDAOImpl implements ProductDAO{

    private Connection connection;
    
    public ProductDAOImpl(Connection connection){
        this.connection = connection;
    }
    
    @Override
    public int save(Product product  ) throws SQLException {
        
        String sql ="insert into product(category_id, name, description, price, expiredDate ) values(?,?,?,?,?)";
        PreparedStatement statement = connection.prepareCall(sql);
        if ( product.getCategory() != null ){
            statement.setInt(1, product.getCategory().getId() );
        }else {
            throw new SQLException( "data category blm ada");
        }
        statement.setString(2, product.getName() );
        statement.setString(3, product.getDescription());
        statement.setDouble(4, product.getPrice());
        statement.setDate(5, new java.sql.Date ( product.getExpiredDate().getTime() ) );
        return statement.executeUpdate();
    }

    @Override
    public int update(int i, Product product) throws SQLException {
        String sql = "UPDATE product SET name = ? , description = ?, price = ?, expiredDate = ?, category_id = ?  WHERE product_id = ? ";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, product.getName());
        statement.setString(2, product.getDescription());
        statement.setDouble(3, product.getPrice());
        statement.setDate(4, new java.sql.Date (product.getExpiredDate().getDate()));
        statement.setInt(5, product.getCategory().getId());
        
        statement.setInt(6, i);
        return statement.executeUpdate();
    }

    @Override
    public int delete(int i) throws SQLException {
        String sql ="delete from product where product_id = ? ";
        
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1, i);
        return preparedStatement.executeUpdate();
    }

    @Override
    public Product findById(int i) throws SQLException {
        String sql =" SELECT p.product_id,p.name,p.description,p.price,p.expireddate,p.category_id,c.name as category_name " +
        " FROM product p INNER JOIN category c ON (p.category_id = c.category_id) " +
        " Where p.product_id = ? ";
        
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, i);
        ResultSet resultSet = statement.executeQuery();
        //List<Product> list = new ArrayList<>();
        
        Product prod = null;
        if (resultSet.next()){
            
            Category cat  = new Category();
            cat.setId(resultSet.getInt("category_id"));
            cat.setName(resultSet.getString("category_name"));
            
            prod = new Product();
            prod.setCategory(cat);
            prod.setDescription(resultSet.getString("description"));
            prod.setExpiredDate(resultSet.getDate("expireddate"));
            prod.setId(resultSet.getInt("product_id"));
            prod.setName(resultSet.getString("name"));
            prod.setPrice(resultSet.getDouble("price"));
            
            //list.add(prod);
        }
        return prod;
    }

    @Override
    public List<Product> findAll() throws SQLException {
        String sql =" SELECT p.product_id,p.name,p.description,p.price,p.expireddate,p.category_id,c.name as category_name " +
                " FROM product p INNER JOIN category c ON (p.category_id = c.category_id)";
        
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet result = statement.executeQuery();
        List<Product> list = new ArrayList<Product>();
        while (result.next()){
            Category cat = new Category();
            cat.setId(result.getInt("category_id"));
            cat.setName(result.getString("category_name"));
            
            Product prod = new Product();
            prod.setCategory(cat);
            prod.setDescription(result.getString("description"));
            prod.setPrice(result.getDouble("price"));
            prod.setExpiredDate(result.getDate("expiredDate"));
            prod.setId(result.getInt("product_id"));
            prod.setName(result.getString("name"));
            
            list.add(prod);
        }
                
        return list;
    }

    @Override
    public List<Product> findByName(String name) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
