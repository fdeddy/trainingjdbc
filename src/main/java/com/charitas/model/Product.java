/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.charitas.model;

import java.util.Date;

/**
 *
 * @author edepe1
 */
public class Product {
    private int id;
    private String name;
    private String description;
    private Category category ;
    private double price;
    private Date expiredDate ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }



    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }
    
    
    
}
