/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.charitas.util;

import java.awt.Container;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 *
 * @author edepe1
 */
public final class SwingUtil {
    
    public static JFrame getParentJFrame(JComponent component){
        Container parent = component;
        do {
            parent = (Container) parent.getParent();
        } while (!( parent instanceof JFrame));
        
        return ((JFrame) parent );
    }
    
}
